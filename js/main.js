let selectX = $('#selectX');
let inputY = $("#textY");
let form = $("#sendD");
let tooltip = $("#tooltipId");
let hiddenR = $("#hiddenR");
let logR = $("#Rlog");
let submitB = $("#submit");

tooltipHidden();
submitB.prop('disabled', true);

setInputFilter(document.getElementById("textY"), (value) => /^-?\d*[.,]?\d*$/.test(value));

for (let i = -4; i < 5; i++)
    selectX.append($('<option>', { value: i, text : i }));

selectX.val("0");

["#buttonR1", "#buttonR2", "#buttonR3", "#buttonR4", "#buttonR5"].forEach(function(event) {
    $(event).on('click', () => {
        hiddenR.val($(event).val());
        logR.html("R = " + $(event).val()) 
    });
});

inputY.on('change', () => checkY(inputY.val().replace(',', '.')));

function setInputFilter(textbox, inputFilter) {
    ["input", 'keydown'].forEach(function(event) {
        textbox.addEventListener(event, function (e) {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
                tooltipHidden(true, "");
            } else if (this.hasOwnProperty("oldValue")) {
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                tooltipHidden(false, "Введете цифры из диапазона 0..9");
            }
        });
    });
}

form.on('submit', function(event){
    event.preventDefault();
    if(!checkY(inputY.val().replace(",", '.'))) return;

    $("#offset").val(new Date().getTimezoneOffset());

    let ser = form.serialize().replace(',', '.');

    let req = new XMLHttpRequest();
    req.open("POST", "check.php", true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            if (req.status === 200) {
                $(".result").attr("srcdoc", req.responseText);
            }
        }
    };
    req.send(ser);
});

function checkY(y) {
    let flag = false;
    if (!(y === "")) {
        if (isNaN(y) && !Number.isFinite(Number(y)) || /^\./.test(y) || /\.$/.test(y)  || /-0$/.test(y)) {
            tooltipHidden(false,"Некорректное значение Y");
        } else {
            if ((Big(y)).gt(Big(-3)) && (Big(y)).lt(Big(3))) {
                flag = true;
                tooltipHidden(true, "");
            } else {
                tooltipHidden(false,'Выход за пределы диапазона (-3;3)');
            }
        }
    }else {
        tooltipHidden(false,'Не введено значение Y');
    }
    return flag;
}
  
function tooltipHidden(flag, text) {
    tooltip.html(text);
    if (flag){
        tooltip.css("visibility", "hidden");
        submitB.prop('disabled', false);
    }else{
        tooltip.css("visibility", "visible");
        submitB.prop('disabled', true);
    }
}