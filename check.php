<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Результат проверки</title>
        <style>
            .success { color: green; }
            .wrong { color: red; }
            td {
                border-top: 1px solid #e8edff;
                padding: 5px 5px;
                text-align: center;
            }
            tr:hover td { background: #e8edff; }
        </style>
    </head>
<body>

<?php
ini_set("precision", 14);
//if (session_id() === "") {s)) { $_SESSION['date'] = array(); }

$timeStart = microtime(true);

date_default_timezone_set("UTC");
if (array_key_exists("offset", $_POST)) {
    $OFFSET = $_POST["offset"];
} else
    $OFFSET = 0;

//date_default_timezone_set("Europe/Moscow");

if(!isset($_POST['X'])){ die('Вы не выбрали переменную X'); }
if(!isset($_POST['Y'])){ die('Вы не выбрали переменную Y'); }
if(!isset($_POST['R'])){ die('Вы не выбрали переменную R'); }

$x = $_POST['X'];
if($x < -5 || $x > 5) { die("Получены неверные данные X: $x"); }

$y = $_POST['Y'];
$y_fl = (float)$y;

if( !(strFloatCompare($y, "-3") > 0 && strFloatCompare($y, "3") < 0) ){ die("Получены неверные данные Y: $y"); }

$r = $_POST['R'];
if($r < 1 || $r > 3){ die("Получены неверные данные R: $r"); }

if(strFloatCompare($y, rtrim(sprintf('%.15f', $y_fl), '0')) !== 0){
    echo "<p>Внимание. Введенное число Y будет округлено. Возможен неккоректный результат</p>";
}

$hit = checkArea($x, $y_fl, $r);

//array_push($_SESSION['date'], array( 'x' => $x, 'y' => $y, 'r' => $r, 'hit' => $hit ));

$x_out = "<td>X</td>";
$y_out = "<td>Y</td>";
$r_out = "<td>R</td>";
$hit_out = "<td>Hit</td>";

//foreach ($_SESSION['date'] as $i => $val) {
//    $x_out .= '<td>'.$val['x'].'</td>';
//    $y_out .= '<td>'.$val['y'].'</td>';
//    $r_out .= '<td>'.$val['r'].'</td>';
//    $hit_out .= '<td class="'.($val['hit'] ? "success" : "wrong").'" id="hit_result">'.($val['hit'] ? "YES" : "NO").'</td>';
//}

$x_out .= '<td>'.$x.'</td>';
$y_out .= '<td>'.preg_replace('/\.$/', '', rtrim(sprintf('%.15f', $y_fl), '0')).'</td>';
$r_out .= '<td>'.$r.'</td>';
$hit_out .= '<td class="'.($hit ? "success" : "wrong").'" id="hit_result">'.($hit ? "YES" : "NO").'</td>';


$echo_table = "<table>
    <tr>$x_out</tr>
    <tr>$y_out</tr>
    <tr>$r_out</tr>
    <tr>$hit_out</tr>
</table>";

echo "<p id='time'>Текущее время сервера: ".date("d.m.Y, G:i:s", $timeStart - $OFFSET / 60 * 3600)." </p>";
echo "<p id='time'>Время работы скрипта: ".round((microtime(true) - $timeStart), 4)." сек<br></p>";
echo $echo_table;

function checkArea($x, $y, $r){
    if($x >= 0 && $y >= 0){ // I квадрант
        return ($x * $x + $y * $y) <= $r*$r;
    }else if ($x <= 0 && $y >= 0) { // II квадрант
        return ($y - $x)*2 <= $r;
    }else if ($x <= 0 && $y <= 0) { // III квадрант
        return ( $y >= -$r/2 ) && ( $x >= -$r );
    }else if ($x >= 0 && $y <= 0) {// IV квадрант
        return false;
    }else{
        return false;
    }
}

function strFloatCompare($first, $second) {
    $first = trim((string) $first);
    $second = trim((string) $second);

    $isNegativeFirst = !(strpos($first, '-') == false);
    $isNegativeSecond = !(strpos($second, '-') == false);

    if($isNegativeFirst && !$isNegativeSecond){ return -1; }
    if(!$isNegativeFirst && $isNegativeSecond){ return 1; }

    $posDotFirst = strpos($first, '.');
    $posDotSecond = strpos($second, '.');

    $firstWole = $first;
    $firstFraction = "";
    $secondWole = $second;
    $secondFraction = "";

    if($posDotFirst !== false){
        $firstWole = substr($first, 0, $posDotFirst);
        $firstFraction = substr($first, $posDotFirst);
    }

    if($posDotSecond !== false){
        $secondWole = substr($second, 0, $posDotSecond);
        $secondFraction = substr($second, $posDotSecond);
    }

    if((int)$firstWole > (int)$secondWole){
        return $isNegativeFirst ? -1 : 1;
    }

    if((int)$firstWole < (int)$secondWole){
        return $isNegativeSecond ? 1 : -1;
    }

    $lenFirstFraction = strlen($firstFraction);
    $lenSecondFraction = strlen($secondFraction);

    $delta = $lenFirstFraction - $lenSecondFraction;

    if($delta > 0){
        $secondFraction .= str_repeat('0', abs($delta));
    }else if($delta < 0){
        $firstFraction .= str_repeat('0', abs($delta));
    }

    for($i = 0; $i < strlen($firstFraction); $i++){
        $fi = (int)($firstFraction[$i]);
        $si = (int)($secondFraction[$i]);
        if($fi > $si) return $isNegativeFirst ? -1 : 1;
        if($fi < $si) return $isNegativeSecond ? 1 : -1;
    }

    return 0;
}

?>

</body>
</html>